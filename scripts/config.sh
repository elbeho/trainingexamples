#!/bin/bash

git config --global user.name 'Juste Untest'
git config --global user.email 'juste.untest@myg.it'

git config --global push.default matching

git config --global alias.lg 'log --all --oneline --graph --decorate'
git config --global alias.clog 'log --all --graph --format="format:%h [%C(cyan)%cn%Creset] %C(auto)%d  %s"'

exit 0


