#!/bin/bash

# 0. Prologue

user_name=$( git config --get user.name )
if [ 'x' = "x${user_name}" ]
then
    echo "ERROR: user.name is not defined. Please check your config."
    exit 1
fi

repo=$1
[ 'x' = "x${repo}" ] && repo=ComponentA
if [ 'x-h' = "x${repo}" ]
then
    echo "usage: $1 [ repo [ dir ] ]

Create an empty Git repository named 'repo' in directory 'dir'.
By default repo is 'ComponentA' and dir is '\$HOME/Bureau'

Prereq: user.name and user.email are configured

"
    exit 0
fi

tgt=$1
[ 'x' = "x${tgt}" ] && tgt="${HOME}/Bureau"


#--------------------------------------
create_release()
{
    tag=$1
	shift
	prefix=$*
	
	git checkout develop
	echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
' > file1.txt
	echo 'Ego vero sic intellego, Patres conscripti, nos hoc tempore in provinciis 
decernendis perpetuae pacis habere oportere rationem. 
' > file2.txt
	mkdir -p dir1
	echo 'Et est admodum mirum videre plebem innumeram mentibus ardore quodam infuso 
cum dimicationum curulium eventu pendentem. haec similiaque memorabile 
nihil vel serium agi Romae permittunt. ergo redeundum ad textum.
' > dir1/file3.txt
	git add .
	git commit -m"$prefix #1"

	echo 'Sed neque mauris, consectetur sit amet dolor et, laoreet ultricies ante.
' >> file1.txt
	echo 'Nam quis hoc non sentit omnia alia esse nobis vacua ab omni periculo 
atque etiam suspicione belli?
' >> file2.txt
	echo "" >> dir1/file3.txt
	date >> dir1/file3.txt
	git add .
	git commit -m"$prefix #2"
	
	echo 'Phasellus tristique eget magna finibus condimentum. 
' >> file1.txt
	date >> file1.txt
	echo '' >> file2.txt
	date >> file1.txt
	date >> dir1/file3.txt

	git add .
	git commit -m"$prefix #3"
}
#--------------------------------------


# 1. Create and initialize the repository
[ -d $tgt ] || mkdir $tgt
cd $tgt
git init ${repo}
cd ${repo}

git commit --allow-empty -m'initial empty commit'

# 2. Create a 1st file
echo 'Welcome to Git
' > Readme.txt
git add Readme.txt
git commit -mReadme

# 3. Create branch
git branch develop

# 4. Create release 1.0
create_release v1.0 "commit for release 1.0"
git checkout master
git merge --no-ff -m'Release 1.0' develop
git tag -a v1.0 -m"This is release 1.0"
git checkout develop
git merge master

# 4. Create release 2.0
create_release v2.0 "commit for release 2.0"
git checkout master
git merge --no-ff -m'Release 2.0' develop
git tag -a v2.0 -m"This is release 2.0"
git checkout develop
git merge master

# 4. Create release 3.0
create_release v3.0 "commit for release 3.0"
git checkout master
git merge --no-ff -m'Release 3.0' develop
git tag -a v3.0 -m"This is release 3.0"
git checkout develop
git merge master

# 4. Create release 4.0
create_release v4.0 "commit for release 4.0"
git checkout master
git merge --no-ff -m'Release 4.0' develop
git tag -a v4.0 -m"This is release 4.0"
git checkout develop
git merge master

exit 0


