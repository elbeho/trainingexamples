#!/bin/bash

# 0. Prologue

user_name=$( git config --get user.name )
if [ 'x' = "x${user_name}" ]
then
    echo "ERROR: user.name is not defined. Please check your config."
    exit 1
fi

repo=$1
[ 'x' = "x${repo}" ] && repo=prj-sample
if [ 'x-h' = "x${repo}" ]
then
    echo "usage: $1 [ repo [ dir ] ]

Create an empty Git repository named 'repo' in directory 'dir'.
By default repo is 'prj-sample' and dir is '\$HOME/Bureau'

Prereq: user.name and user.email are configured

"
    exit 0
fi

tgt=$2
[ 'x' = "x${tgt}" ] && tgt="${HOME}/Bureau"




# 1. Create and initialize the repository
[ -d $tgt ] || mkdir $tgt
cd $tgt
git init ${repo}
cd ${repo}

git commit --allow-empty -m'initial empty commit'

# 2. Create a 1st file
echo 'Welcome to Git
' > Readme.txt
git add Readme.txt
git commit -mReadme

# 3. Develop
git checkout -b develop
echo 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Sed neque mauris, consectetur sit amet dolor et, laoreet ultricies ante.
Phasellus tristique eget magna finibus condimentum. 
' > file1.txt
echo 'Ego vero sic intellego, Patres conscripti, nos hoc tempore in provinciis 
decernendis perpetuae pacis habere oportere rationem. Nam quis hoc non 
sentit omnia alia esse nobis vacua ab omni periculo atque etiam 
suspicione belli?
' > file2.txt
mkdir dir1
echo 'Et est admodum mirum videre plebem innumeram mentibus ardore quodam infuso 
cum dimicationum curulium eventu pendentem. haec similiaque memorabile 
nihil vel serium agi Romae permittunt. ergo redeundum ad textum.
' > dir1/file3.txt

git add .
git commit -m'first 3 files'

exit 0


